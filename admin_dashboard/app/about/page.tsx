import React from 'react'
import Link from 'next/link';

const aboutPage = () => {
  return <div>
      <h1>
      About
      </h1>
    <h2>
        <Link href="/">← Back to home</Link>
    </h2>
    </div>;
};

export default aboutPage;
