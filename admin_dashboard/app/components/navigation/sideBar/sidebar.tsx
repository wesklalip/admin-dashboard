// components/Sidebar.js
import React from 'react';
import Link from 'next/link';
import searchBy from '@/app/searchBy/page';

const Sidebar = () => {
  return (
    <aside style={{ 
      backgroundColor: '#555', // Choose your desired background color
      color: '#fff',          // Choose your desired text color
      padding: '1rem',        // Adjust padding as needed
      width: '100px',         // Set the width of the sidebar
      position: 'bottom',      // Fixed positioning to keep it on the right side
      top: 0,                 // Align it to the top of the viewport
      right: 0,               // Align it to the right of the viewport
      height: '100vh',        // Set the height to fill the viewport
    }}>
      <Link href="./searchBy" 
      style={{ margin: '0 1rem', 
      textDecoration: 'none', 
      color: '#fff' }}>searchBy
      </Link>
      {/* Your sidebar content */}
    </aside>
  );
};

export default Sidebar;