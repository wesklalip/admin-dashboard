// components/Navbar.js
import React from 'react';
import Link from 'next/link';
import aboutPage from '@/app/about/page';
import Contact from '@/app/contact/page';
import Services from '@/app/services/page';
import Login from '@/app/login/page';
import Image from 'next/image';

const Navbar = () => {
  return (
    <nav style={{ 
      backgroundColor: '#333',  // Choose your desired background color
      color: '#fff',           // Choose your desired text color
      padding: '1rem',         // Adjust padding as needed
      display: 'flex',         // Use flexbox to arrange items horizontally
      justifyContent: 'right', // Center the items horizontally
      alignItems: 'center',     // Center the items vertically
    }}>
      <div className='container'> 
        <Image
        src="/images/censLogo.png"
        alt="Logo"
        width={70} // Set the width of the image
        height={70} // Set the height of the image
      />
      </div>
      <div>
        <p>
          2024 CENSUS CAPI MONITORING DASHBORD
        </p>
      </div>

       {/* About Page */}
       <div>
       <Link href="./about" style={{ margin: '0 1rem', textDecoration: 'none', color: '#fff' }}>About
      </Link> 
       </div>
       {/* Contact Page */}
      <div>
      <Link href="./contact" style={{ margin: '0 1rem', textDecoration: 'none', color: '#fff' }}>Contact
      </Link>
      </div>
      {/* Services Page */}
      <div>
       <Link href="./services" style={{ margin: '0 1rem', textDecoration: 'none', color: '#fff' }}>Services
      </Link> 
      </div>
      {/* Login Page */}
      <div>
      <Link href="./login" style={{ margin: '0 1rem', textDecoration: 'none', color: '#fff' }}>Login
      </Link>
      </div>
      {/* Home Page */}
      <div style={{ display: 'flex'}}>
          <Link href="/" style={{ margin: '0 1rem', textDecoration: 'none', color: '#fff' }}> Home
      </Link>
      </div>
    </nav>
  );
};

export default Navbar;