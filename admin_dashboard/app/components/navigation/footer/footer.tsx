// components/Footer.js
import React from 'react';
import './footer-style.css';


const Footer = () => {
  return (
    <footer className="footer">
      <div className="container"> 
        <h1>
          What is Lorem Ipsum?
        </h1>
        <p>
          Lorem Ipsum is simply dummy text of the printing 
          and typesetting industry. Lorem Ipsum has been the 
          industry's standard dummy text ever since the 1500s, 
          when an unknown printer took a galley of type and 
          scrambled it to make a type specimen book.
        </p>
      </div>
      <div className="container">
        <p>&copy; 2024 Your Company Name. All rights reserved.</p>
      </div>
    </footer>
  );
};

export default Footer;