import React from 'react';
import Layout from './layout'
import './globals.css';
import Footer from './components/navigation/footer/footer';

const Home = () => {
  return (
    <Layout>
      <h1>Admin Dashboard</h1>
      {/* Your page content */}
    </Layout>
  );
};

export default Home;