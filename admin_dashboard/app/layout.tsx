// components/Layout.js
import React from 'react';
import Navbar from './components/navigation/navBar/navbar';
import Sidebar from './components/navigation/sideBar/sidebar';
import Footer from './components/navigation/footer/footer';

const Layout = ({ children }) => {
  return (
    <div style={{ 
      display: 'flex',       // Use flexbox
      flexDirection: 'column', // Arrange items in a column
      height: '100vh',        // Set the height to fill the viewport
    }}>
      {/**********NavBar************/}
      <Navbar />
      <div style={{ 
        display: 'flex',       // Use flexbox
        flexDirection: 'row',  // Arrange items in a row
        flex: 1,                // Allow the main content to take remaining space
      }}>
        {/**********SideBar************/}
        <Sidebar /> 
      <div style={{ 
        display: 'flex',       // Use flexbox
        flexDirection: 'row',  // Arrange items in a row
        flex: 1,                // Allow the main content to take remaining space
      }}>
        {/**********Footer************/}
        <Footer /> 
      <div style={{ 
        display: 'flex',       // Use flexbox
        flexDirection: 'row',  // Arrange items in a row
        flex: 1,                // Allow the main content to take remaining space
      }}></div>
        {/**********Children Page************/}
        <main style={{ 
          flex: 1,              // Allow the main content to take remaining space
          padding: '1rem',      // Add padding as needed
        }}>
          {children}
        </main>
      </div>
    </div>
  </div>
  );
};

export default Layout;